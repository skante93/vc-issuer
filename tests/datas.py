vc_agdatahub = {
  "@context": [
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials", 
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020"
  ],
  "@type": [
      "VerifiableCredential"
  ],
  "@id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/4cef2cda269bf8e1386f8dfa907f48f99e84df01fbaf7d764db586bf746ec8fd/data.json",
  "issuer": "did:web:agdatahub.provider.demo23.gxfs.fr",
  "credentialSubject": {
      "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/9b29f510ce20317bd550c537c5e06cd87644403b619fab1f2c7c1b493b2c2d2f/data.json",
      "type": "gx:ServiceOffering",
      "gx:providedBy": {
          "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/legalperson/data.json"
      },
      "gx:termsAndConditions": {
          "gx:URL": "https://agdatahub.provider.demo23.gxfs.fr/participant/eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/gaiax-terms-and-conditions.json",
          "gx:hash": "a452d70abc5870d06cb9482c41d62c1fde03bc166ca6d5dddc9133358fd88866"
      },
      "gx:name": "test",
      "aster-conformity:layer": "IAAS",
      "gx:keyword": [
          "Authentication"
      ],
      "gx:description": "dsqd",
      "gx:descriptionMarkDown": "",
      "gx:webAddress": "",
      "gx:dataProtectionRegime": [
          "GDPR2016"
      ],
      "gx:dataAccountExport": [
          {
              "gx:requestType": "email",
              "gx:accessType": "digital",
              "gx:formatType": "mime/png"
          }
      ],
      "gx:dependsOn": "",
      "gx:isAvailableOn": [
          ""
      ]
  }
}
