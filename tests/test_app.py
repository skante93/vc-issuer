import json
from jwcrypto import jwk, jws
from pyld import jsonld
from hashlib import sha256
from vc_issuer.app import app
from vc_issuer import config, utils
from tests import datas

def sign(client, document, alg):
    #Signature
    response = client.put(f'/api/v0.9/sign?{config.ARG_ALG_TYPE}={alg}', json=document)

    return response

def verify(document_signed):
    jws_value = document_signed.get("proof").get("jws")

    #Verification step 1 : get info from jws
    protected_b64, payload_b64, signature_b64 = jws_value.split('.')
    jws_dict = {
        "protected": protected_b64,
        "payload": payload_b64,
        "signature": signature_b64
    }
    jws_serialized = json.dumps(jws_dict)
    jwstoken = jws.JWS()
    jwstoken.deserialize(jws_serialized)

    #Verification, step 2 : get public key from private key
    with open(config.PRIVATE_KEY_PATH, "rb") as file:
        private_key = jwk.JWK.from_pem(file.read())
        public_key = private_key.public()

    #Verification, step 3 : recreate payload (detached payload is not included in jws)
    credential = document_signed.copy()
    del credential["proof"]
    credential_normalized = jsonld.normalize(credential, {"algorithm": "URDNA2015", "format": "application/n-quads"})
    # Recreate empty proof with context
    proof = document_signed["proof"].copy()
    proof["@context"] = document_signed.get('@context', config.VC_CONTEXT)
    del proof["jws"]
    proof_normalized = jsonld.normalize(proof, {"algorithm": "URDNA2015", "format": "application/n-quads"})
    proof_hashed = sha256(utils.string_to_bytes(proof_normalized)).digest()
    credential_hashed = sha256(utils.string_to_bytes(credential_normalized)).digest()
    payload_tosign = proof_hashed + credential_hashed

    try:
        jwstoken.verify(public_key, detached_payload=payload_tosign)
        return True
    except Exception as e:
        print(e)
    return False

def test_hello_world(client):
    response = client.get('/')
    assert response.status_code == 200
    assert b"Hello VC-Issuer!" in response.data

def test_sign_ec(client):
    is_ec = config.KTY_EC.lower() in config.PRIVATE_KEY_PATH

    if is_ec:
        for alg in config.SUPPORTED_EC_SIGNATURE_ALGS:
            response = sign(client=client, document=datas.vc_agdatahub, alg=alg)
            if alg == config.DEFAULT_SIGNATURE_ALG:
                assert response.status_code == 200
                assert verify(response.json) == True
            else:
                assert response.status_code == 400
    else:
        for alg in config.SUPPORTED_EC_SIGNATURE_ALGS:
            response = sign(client=client, document=datas.vc_agdatahub, alg=alg)
            assert response.status_code == 400
            
def test_sign_rsa(client):
    is_rsa = config.KTY_RSA.lower() in config.PRIVATE_KEY_PATH

    if is_rsa:
        for alg in config.SUPPORTED_RSA_SIGNATURE_ALGS:
            response = sign(client=client, document=datas.vc_agdatahub, alg=alg)
            assert response.status_code == 200
            assert verify(response.json) == True
    else:
        for alg in config.SUPPORTED_RSA_SIGNATURE_ALGS:
            response = sign(client=client, document=datas.vc_agdatahub, alg=alg)
            assert response.status_code == 400

def test_sign_okp(client):
    is_okp = config.KTY_OKP.lower() in config.PRIVATE_KEY_PATH

    if is_okp:
        for alg in config.SUPPORTED_OKP_SIGNATURE_ALGS:
            response = sign(client=client, document=datas.vc_agdatahub, alg=alg)
            if alg == config.DEFAULT_SIGNATURE_ALG:
                assert response.status_code == 200
                assert verify(response.json) == True
            else:
                print(response.json)
                assert response.status_code == 400
    else:
        for alg in config.SUPPORTED_OKP_SIGNATURE_ALGS:
            response = sign(client=client, document=datas.vc_agdatahub, alg=alg)
            assert response.status_code == 400

def test_normalize(client):
    response = client.put('/api/v0.9/normalize', json=datas.vc_agdatahub)
    assert response.status_code == 200
    
    normalized_original = jsonld.normalize(datas.vc_agdatahub, {"algorithm": "URDNA2015", "format": "application/n-quads"})
    hashed_normalized_original = sha256(utils.string_to_bytes(normalized_original)).digest()
    hashed_normalized_response = sha256(response.data).digest()

    assert hashed_normalized_original == hashed_normalized_response

def test_status(client):
    response = client.get('/api/v0.9/status')
    assert response.status_code == 200