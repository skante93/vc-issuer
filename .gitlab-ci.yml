include:
  # Docker template
  - project: "to-be-continuous/docker"
    ref: "5.8"
    file: "templates/gitlab-ci-docker.yml"
  # Python template
  - project: "to-be-continuous/python"
    ref: "6.7"
    file: "templates/gitlab-ci-python.yml"
  # semantic-release template
  - project: "to-be-continuous/semantic-release"
    ref: "3.8"
    file: "templates/gitlab-ci-semrel.yml"

variables:
  TERM: "xterm-256color"
  PROD_REF: '/^(main)$/'
  INTEG_REF: '/^develop$/'

  PYTHON_IMAGE: "${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/python:3.11"
  PYTHON_PROJECT_DIR: "."
  PYTHON_BUILD_SYSTEM: "poetry"
  PYTHON_PACKAGE_ENABLED: "true"
  PYLINT_ENABLED: "false"
  PYLINT_ARGS: "--rcfile=./pyproject.toml"
  PYLINT_FILES: "vc_issuer"
  UNITTEST_ENABLED: "false"
  PYTEST_ENABLED: "true"
  PYTEST_ARGS: "tests/"
  NOSETESTS_ENABLED: "false"
  BANDIT_ENABLED: "false"
  BANDIT_ARGS: "-lll --recursive vc_issuer"
  PYTHON_TRIVY_ENABLED: "false"
  PYTHON_SBOM_DISABLED: "true"
  PYTHON_RELEASE_ENABLED: "false"
  PYTHON_SEMREL_RELEASE_DISABLED: "false"
  PYTHON_BLACK_ENABLED: "false"
  PYTHON_ISORT_ENABLED: "false"
  KANIKO_SNAPSHOT_IMAGE_CACHE: "${CI_REGISTRY_IMAGE}/snapshot"

  SAFETY_ENABLED: "false"
  DOCKER_BUILD_TOOL: "kaniko"
  DOCKER_BUILD_CACHE_DISABLED: "false"
  DOCKER_KANIKO_IMAGE: "gcr.io/kaniko-project/executor:v1.9.0-debug"
  DOCKER_HADOLINT_DISABLED: "false"
  DOCKER_HADOLINT_IMAGE: "${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/hadolint/hadolint:v2.12.0-alpine"
  DOCKER_HEALTHCHECK_DISABLED: "true"
  DOCKER_TRIVY_DISABLED: "false"
  DOCKER_TRIVY_IMAGE: "${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/aquasec/trivy:latest"
  DOCKER_TRIVY_ARGS: "--ignore-unfixed --vuln-type os"
  DOCKER_TRIVY_SECURITY_LEVEL_THRESHOLD: "CRITICAL"
  DOCKER_BUILD_ARGS: "--cache-ttl=6h --build-arg CI_PROJECT_URL"
  DOCKER_SBOM_DISABLED: "false"
  DOCKER_SBOM_IMAGE: "${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/anchore/syft:debug"
  DOCKER_SEMREL_RELEASE_DISABLED: "false"
  DOCKER_RELEASE_EXTRA_TAGS: "latest \\g<major>.\\g<minor>\\g<build> \\g<major>\\g<build>"
  DOCKER_PROD_PUBLISH_STRATEGY: "manual"

  SEMREL_IMAGE: "${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/node:latest"
  SEMREL_CHANGELOG_ENABLED: "true"
  SEMREL_RELEASE_DISABLED: "false"
  SEMREL_INFO_ON: "prod"
  SEMREL_DRY_RUN: "false"
  SEMREL_AUTO_RELEASE_ENABLED: "false"

stages:
  - build
  - test
  - package-build
  - package-test
  - infra
  - deploy
  - acceptance
  - publish
  - infra-prod
  - production

.python-base:
    tags:
      - gxfs-fr

.docker-base:
    tags:
      - gxfs-fr

.semrel-base:
    tags:
      - gxfs-fr

#integration_tests:
#  variables:
#    DOCKER_HOST: tcp://docker:2376
#    #DOCKER_DRIVER: overlay2
#    DOCKER_TLS_VERIFY: "1"
#    DOCKER_CERT_PATH: "${DOCKER_TLS_CERTDIR}/client"
#    DOCKER_TLS_CERTDIR: "/certs"
#  services:
#  - docker:25.0.3-dind
#  stage: "test"
#  tags:
#    - gxfs-fr
#  image:
#    name:  ${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/docker:25.0.3
#    entrypoint: [""]
#  before_script:
#    - until docker info; do sleep 1; done
#  script:
#    - docker-compose --env-file code/.test.env -f docker-compose-ci.yml up --renew-anon-volumes --exit-code-from pytest

deploy:dev:
  stage: "deploy"
  image:
    name: ${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/nixos/nix:2.20.4
    entrypoint: [""]
  variables:
    PACKAGE_PATH: "deployment/packages/overlays/dev-participants"
  script:
    - nix-env -iA nixpkgs.kustomize nixpkgs.kubectl
    - kubectl config get-contexts
    - kubectl config use-context gaia-x/data-infrastructure-federation-services/deployment-scenario/infrastructure/k8s-deployment:dev
    # Update the kustomize overlay with the image reference.
    - cd ${PACKAGE_PATH}
    - kustomize edit set image image-name=${DOCKER_SNAPSHOT_IMAGE}
    # Generate the definitive kubernetes manifests using the kustomize overlay
    - kubectl apply -k . --timeout=300s --wait=true
    - kubectl delete pods --selector app=vc-issuer  --all-namespaces
  rules:
      - if: $CI_COMMIT_BRANCH == 'develop'
  tags:
    - gxfs-fr

deploy:demo23:
  stage: "production"
  image:
    name: ${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/nixos/nix:2.20.4
    entrypoint: [""]
  variables:
    PACKAGE_PATH: "deployment/packages/overlays/demo23-participants"
  script:
    - nix-env -iA nixpkgs.kustomize nixpkgs.kubectl
    - kubectl config get-contexts
    - kubectl config use-context gaia-x/data-infrastructure-federation-services/deployment-scenario/infrastructure/k8s-deployment:demo23
    - kubectl get pods -n gitlab-agent-dev
    # Update the kustomize overlay with the image reference.
    - cd ${PACKAGE_PATH}
    - echo ${CI_REGISTRY_IMAGE}:${SEMREL_INFO_NEXT_VERSION}
#    - kustomize edit set image image-name=${DOCKER_RELEASE_IMAGE}
    - kustomize edit set image image-name=${CI_REGISTRY_IMAGE}:${SEMREL_INFO_NEXT_VERSION}
    # Generate the definitive kubernetes manifests using the kustomize overlay
    - kubectl apply -k . --timeout=300s --wait=true
  rules:
      - if: $CI_COMMIT_BRANCH == 'main'
  tags:
    - gxfs-fr
