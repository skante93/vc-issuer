import shutup; shutup.please()

import os, logging, json, pytz, ssl, httpx
import config, utils
from typing import List, Union
from flask import Flask, request, Request
from pyld import jsonld
from dotenv import load_dotenv
from hashlib import sha256
from datetime import datetime, timedelta
from jwcrypto import jwk, jws
from jwcrypto.common import json_encode

############
# MAIN APP #
############
app = Flask(__name__)

load_dotenv()
logging.basicConfig(level=logging.DEBUG)
logging.getLogger()

app.did_web_issuer = config.DID_WEB_ISSUER
app.logger.info ("Issuer Did Web successfully configured")

app.algoType = config.DEFAULT_SIGNATURE_ALG
app.logger.info(f"Default signature type is set to {app.algoType}")

app.pk_path = config.PRIVATE_KEY_PATH
if not os.path.exists(app.pk_path):
    err = f"File {app.pk_path} doesn't exist. Impossible to read private key"
    app.logger.error(err)
    exit(-1)
try:
    file = open(app.pk_path, "rb")
    app.private_key = jwk.JWK.from_pem(file.read())
    app.key_type = app.private_key.key_type
    app.key_curve = utils.get_curve(app.private_key)
    app.has_key = True
except FileNotFoundError as e:
    err = f"Reading error with file {e.filename}"
    app.logger.error(err)
    exit(-1)
except Exception as exc :
    err = f"Unable to load private key in file {exc}"
    app.logger.error(err)
    exit(-1)
app.logger.info(f"Signing key is successfully configured with given path {app.pk_path}")

try:
    utils.check_kty_alg_crv(key=app.private_key, alg=app.algoType)
except Exception as e:
    app.logger.error(e)
    exit(-1)
app.logger.info(f"Key type ({app.key_type}) and Alg type ({app.algoType}) are compatible.")

################
# Cache config #
################
import requests_cache
requests_cache.install_cache("context_cache")
def wrapper(url, options, **kwargs):    
    return jsonld.requests_document_loader(timeout=10)(url, options)
jsonld.set_document_loader(wrapper)

#############
# Functions #
#############
def manage_input(request: Request) -> List[Union[dict, str]]:
    '''
    Manage input request
    '''
    document = request.get_json()
    for arg in request.args:
        app.logger.debug(f"{arg}: {request.args.get(arg)}")
    
    algoType = request.args.get(config.ARG_ALG_TYPE, type=str)
    if algoType:
        #Check and raise error if no compatibility
        utils.check_kty_alg_crv(key=app.private_key, alg=algoType)
        app.logger.debug(f"Signature type is specified, {algoType} will be used")
    else:
        algoType = app.algoType
        app.logger.debug(f"No signature type is specified, default {algoType} will be used")

    return [document, algoType]

def get_credential_status() -> dict:
    '''
    Create credential status for credential
    '''
    with httpx.Client() as client:
        revocation_url = config.REVOCATION_URL
        issuer_name= config.ISSUER_NAME
        app.logger.debug(revocation_url)
        try:
            app.logger.debug(f"Sending request to {revocation_url}/api/v1/revocations/statusEntry?issuerId={issuer_name}")

            response = client.post(
                url=f"{revocation_url}/api/v1/revocations/statusEntry?issuerId={issuer_name}",
                json={
                    "credentialUrl":f"{revocation_url}/api/v1/revocations/credentials",
                    "purpose":"revocation"
                },
                timeout= 60.0,
                headers={
                    "Content-Type": "application/json"
                    }
            )

            app.logger.debug(f"Response status code => {response.status_code}")
            app.logger.debug(f"Response content => {response.content}")

            if response.status_code in (200, 201):
                return response.json()
            else:
                raise Exception(response.text)
        except (ssl.SSLCertVerificationError, httpx.ConnectError) as err:
            raise Exception(f"{config.REVOCATION_URL} is not reachable") from err
    
def calc_normalize(document: dict) -> str:
    '''
    Normalize json object
    '''
    return jsonld.normalize(document, {"algorithm": "URDNA2015", "format": "application/n-quads"})

def calc_proof_normalize(document: dict, ctx: dict=config.VC_CONTEXT) -> str:
    '''
    Normalize json proof object
    '''
    proof = document.copy()
    #Delete jws if present
    if proof.get('jws'):
        del proof['jws']
    #Add context
    proof['@context'] = ctx
    return calc_normalize(proof)

def check_rules(document: dict) -> dict:
    '''
    Check severals rules :
     - check if type attribute is valid (is "@type" or "type")
     - check if type is valid (is a VerifiableCredential or a VerifiablePresentation)
     - check if vc is valid (has a credentialSubject and has the right issuer)
     - check if vp is valid (has a verifiableCredential)
    '''
    is_vc = False
    is_vp = False
    has_credential_subject = False
    has_verifiable_credential = False
    has_right_issuer = False
    is_valid = False

    if isinstance(document, dict):
        document_type = document.get("type", document.get("@type"))
        if isinstance(document_type, str):
            document_type = [document_type]
        if isinstance(document_type, list):
            for t in document_type:
                if t.lower() == "verifiablecredential":
                    is_vc = True
                    break
                elif t.lower() == "verifiablepresentation":
                    is_vp = True
                    break

    if is_vc and "credentialSubject" in document:
        has_credential_subject = True
    
    if is_vp and "verifiableCredential" in document:
        has_verifiable_credential = True

    # Disable rule of issuer
    # if obj.get("issuer") == app.did_web_issuer:
    has_right_issuer = True

    if document.get("issuanceDate"):
        document.pop("issuanceDate")
        
    if is_vc:
        is_valid = has_credential_subject and has_right_issuer
        
    if is_vp:
        is_valid = has_verifiable_credential

    if is_vc and is_valid:
        ret = { "value": True, "msg": "OK", "vc": document}
    elif is_vp and is_valid:
        ret = { "value": True, "msg": "OK", "vp": document}
    else:
        if is_vc:
            msg = f"Check Rules Failed for given VC: has_credential_subject({has_credential_subject}), has_right_issuer({has_right_issuer}, expect {app.did_web_issuer})."
        elif is_vp:
            msg = f"Check Rules Failed for given VP: has_verifiable_credential({has_verifiable_credential})."
        else:
            msg = f"Document is neither a VC nor a VP: has_credential_subject({has_credential_subject}), has_right_issuer({has_right_issuer}, expect {app.did_web_issuer}), has_verifiable_credential({has_verifiable_credential})."
        ret = {
            "value": False, 
            "msg": msg
        }

    return ret


##########
# Routes #
##########
@app.route('/')
def hello_world():  # put application's code here
    app.logger.info("Hello VC-Issuer returned")
    return 'Hello VC-Issuer! To get full service status HTTP GET ./api/v0.9/status'

@app.route('/api/sign', methods=['PUT'])
@app.route('/api/v0.9/sign', methods=['PUT'])
def sign():
    is_vc, is_vp = False, False
    
    try:
        [document, algoType] = manage_input(request)
        
        if algoType not in config.SUPPORTED_ALGS_BY_KTY_DICT.get(app.key_type):
            msg = f"Algo type ({algoType}) is not supported for key type ({app.key_type}). (Supported: {config.SUPPORTED_ALGS_BY_KTY_DICT.get(app.key_type)})"
            app.logger.error(msg)
            response = app.response_class(
                response=json.dumps({"msg": msg}, indent=2),
                status=400,
                mimetype="application/json"
            )
            return response

        checking = check_rules(document)

        if not checking["value"]:
            return app.response_class(
                response=json.dumps({"msg": f"Object didn't pass check", "cr_msg": checking["msg"]}),
                status=400,
                mimetype="application/json"
            )
        else:
            if "vc" in checking:
                is_vc = True
                document_to_sign = checking["vc"]
                #app.logger.debug(f"Show VC to normalize: \n{document_to_sign}\n")

            elif "vp" in checking:
                is_vp = True
                document_to_sign = checking["vp"]
                #app.logger.debug(f"Show VP to normalize: \n{document_to_sign}\n")
                
            else:
                return app.response_class(
                    response=json.dumps({"msg": f"Object is neither a VC nor a VP"}),
                    status=400,
                    mimetype="application/json"
                )

        if not app.private_key: 
            app.logger.error("Issuer has no signing Key")
            return app.response_class(
                    response=json.dumps({ "msg": "Issuer has no signing key"}, indent=2),
                    status=400,
                    mimetype='application/json'
                )

        proof = {
            "type": 'JsonWebSignature2020',
            "proofPurpose": 'assertionMethod',
            "verificationMethod": app.did_web_issuer,
        }

        days_for_expiration = config.EXPIRATION_DAYS
        current_time = datetime.now(pytz.utc)
        my_time= current_time.isoformat()
        
        if is_vc:
            expiration_date= (current_time + timedelta(days=days_for_expiration)).isoformat()
            document_to_sign["expirationDate"] = expiration_date
            document_to_sign["issuanceDate"] = my_time
            document_to_sign["credentialStatus"] = get_credential_status()
        proof["created"] = my_time
        
        normalized_document = calc_normalize(document_to_sign)
        ctx = document_to_sign.get("@context", config.VC_CONTEXT)
        normalized_proof = calc_proof_normalize(proof, ctx)
        
        header = {"alg": algoType, "b64": False, "crit": ["b64"]}
        #header_str_nospace = json.dumps(header).replace(" ", "")

        proof_hashed = sha256(utils.string_to_bytes(normalized_proof)).digest()
        app.logger.debug(f"Proof hashed: {proof_hashed.hex()}")
        document_hashed = sha256(utils.string_to_bytes(normalized_document)).digest()
        app.logger.debug(f"Document hashed: {document_hashed.hex()}")
        payload_tosign = proof_hashed + document_hashed

        jwstoken = jws.JWS(payload_tosign)
        jwstoken.add_signature(app.private_key, protected=json_encode(header))
        jwstoken.detach_payload()
        jws_serialized = jwstoken.serialize()
        jws_dict = json.loads(jws_serialized)
        jws_value = jws_dict.get('protected') + "." + jws_dict.get('payload') + "." + jws_dict.get('signature')

        proof['jws'] = jws_value
        document_to_sign["proof"] = proof
    except Exception as e:
            app.logger.error(e)
            return app.response_class(
                response=json.dumps({"msg": str(e)}, indent=2),
                status=400,
                mimetype="application/json"
            )

    return app.response_class(
        response=json.dumps(document_to_sign, indent=2),
        status=200,
        mimetype='application/json'
    )

@app.route('/api/normalize', methods=['PUT'])
@app.route('/api/v0.9/normalize', methods=['PUT'])
def normalize():
    try:
        [document_to_normalize, _] = manage_input(request)
        normalized_obj = calc_normalize(document_to_normalize)
    except Exception as e:
        app.logger.error(e)
        return app.response_class(
            response=json.dumps({"msg": str(e)}, indent=2),
            status=400,
            mimetype="application/json"
        )
    return app.response_class(
        response=normalized_obj,
        status=200,
        mimetype='text/plain'
    )

@app.route('/api/status',methods=['GET'])
@app.route('/api/v0.9/status',methods=['GET'])
def get_status():
    status = dict()
    
    status["pk_loaded"] = app.has_key
    status["pk_path"] = config.PRIVATE_KEY_PATH
    status["log_file"] = config.LOG_FILE_NAME
    status["keyType"] = app.key_type
    status["keyCurve"] = app.key_curve
    status["algoType"] = config.DEFAULT_SIGNATURE_ALG
    status['algos_supported'] = config.SUPPORTED_ALGS_BY_KTY_DICT
    status['did_web_issuer'] = config.DID_WEB_ISSUER
    status['protected_proof_options'] = config.PROTECTED_PROOF_OPTIONS
    status['detached_header'] = config.DETACHED_PAYLOAD
    status['protected_header'] = config.PROTECTED_HEADER
    
    return app.response_class(
        response=json.dumps(status,indent=2),
        status=200,
        mimetype="application/json"
    )


###########
# Run App #
###########
if __name__ == '__main__':
    app.run(debug=False, port=config.FLASK_PORT, host=config.FLASK_HOST)